"""
@file simonsays.py
@brief A program to play simon says with the Nucleo L476
@details Blinks a random pattern for the user to attempt to recreate.\n\n
Source code can be found at https://bitbucket.org/reluttre/lab3/src/master\n\n
State diagram can be found at https://imgur.com/a/YA0VEv2\n\n
Video demonstration can be found at https://youtu.be/xRWqlIRq3ho

@author Robert Luttrell
@date 02/14/2021
"""
import utime
import pyb
import micropython
import math
import random

OFF_TIME_MS = 500
PATTERN_LEN = 6
PATTERN_START_LEN = 2
PATTERN_LEN_INCR = 1
USER_DIFF_TOL = 300
DELAY_TIMEOUT = 3000

def printWelcome():
    """
    @brief Prints a welcome message
    """
    print("Welcome to Simon Says.")
    print("Wait for the pattern to end, then try to replicate the pattern.")
    input("Press Enter to continue...")

def button_callback(IRQ_SRC):
    """
    @brief Button press callback function
    @param IRQ_SRC Interrupt request source
    @details Triggered by rising or falling edge. Creates a new tick for button press time or button release time
    """
    global button_pressed
    global new_button_hold
    global tick_press
    global tick_release
    button_pressed = not button_pressed

    if button_pressed:
        tick_press = utime.ticks_ms()
    else:
        tick_release = utime.ticks_ms()
        new_button_hold = True

def make_pattern(pattern_length):
    """
    @brief Creates a pattern of random selections of 100 or 1000 as an integer
    @param pattern_length length for the pattern
    """
    patt = [random.choice([100, 1000]) for i in range(pattern_length)]
    return patt

def light_for_time(lit_time_ms):
    """
    @brief Lights the nucleo board's LED for a specific time
    @param lit_time_ms duration for the light to be lit
    """
    tick1 = utime.ticks_ms()
    led_pin.value(1)
    tick2 = utime.ticks_ms()

    while utime.ticks_diff(tick2, tick1) < lit_time_ms:
        tick2 = utime.ticks_ms()

    led_pin.value(0)

def display_pattern(ref_patt):
    """
    @brief Displays a pattern using the nucleo board's LED
    @param ref_patt Array of integers representing each duration for LED lighting
    """

    for lit_time_ms in ref_patt:
        light_for_time(lit_time_ms)
        utime.sleep_ms(OFF_TIME_MS)

def read_pattern(pattern_length):
    """
    @brief Reads a pattern from the nucleo board's button
    @param pattern_length length of the pattern to be read
    @details Considers a pattern complete if timeout exceeded
    """
    global new_button_hold
    read_patt = []
    new_button_hold = False
    while len(read_patt) < pattern_length:
        if len(read_patt) > 0 and abs(utime.ticks_diff(tick_release, utime.ticks_ms())) > DELAY_TIMEOUT:
            print("Timeout exceeded. Comparing pattern before timeout.\n")
            return [10000] * pattern_length
        if new_button_hold:
            read_patt.append(utime.ticks_diff(tick_release, tick_press))
            new_button_hold = False

    return read_patt

def get_patt_diffs(patt1, patt2):
    """
    @brief Creates an array of difference magnitudes between input arrays
    @param patt1 first pattern to compare
    @param patt2 second pattern to compare
    @details Array arguments must be same length and of type integer
    """
    diffs = []
    for i in range(len(patt1)):
        diffs.append(abs(patt1[i] - patt2[i]))

    return diffs

def diffs_in_tol(diff_list, tol):
    """
    @brief Determines whether or not all entries in diff_list fall within specified tolerance tol
    @param diff_list array of differences to check
    @param tol tolerance that must be met
    @details Returns true if entries within tolerance, else false
    """
    for diff in diff_list:
        if diff > tol:
            return False

    return True

if __name__ == "__main__":
    micropython.alloc_emergency_exception_buf(100)
    pinC13 = pyb.Pin(pyb.Pin.cpu.C13)
    led_pin = pyb.Pin(pyb.Pin.cpu.A5, pyb.Pin.OUT_PP)
    button_press_int = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING, pull=pyb.Pin.PULL_NONE, callback=button_callback)

    button_pressed = False
    new_button_hold = False
    tick_press = None
    tick_release = None
    cur_pattern_len = PATTERN_START_LEN
    wins = 0
    losses = 0
    state = 0

    while True:
        try:
            if state == 0:
                printWelcome()
                state = 1

            elif state == 1:
                ref_patt_full = make_pattern(PATTERN_LEN)
                cur_pattern_len = PATTERN_START_LEN
                state += 1

            elif state == 2:
                # Display partial pattern
                print("Displaying pattern.\n")
                ref_patt_partial = ref_patt_full[0:cur_pattern_len]
                action = ""
                while action != "c":
                    display_pattern(ref_patt_partial)
                    action = input("Type c to continue or press enter to see pattern again.\n")

                state += 1

            elif state == 3:
                # Read pattern from user
                print("Try to replicate pattern with button.\n")
                input_patt = read_pattern(cur_pattern_len)
                state += 1

            elif state == 4:
                diffs = get_patt_diffs(ref_patt_partial, input_patt)
                patterns_match = diffs_in_tol(diffs, USER_DIFF_TOL)

                if not patterns_match:
                    print("Patterns do not match.\n")
                    losses += 1
                    state = 5

                elif cur_pattern_len < PATTERN_LEN:
                    print("Patterns match. Increasing length\n")
                    cur_pattern_len += 1
                    state = 2

                else:
                    print("You replicated the whole pattern.")
                    wins += 1
                    state = 5

            elif state == 5:
                action = input("Type c to continue or q to quit, followed by enter.\n")
                if action == "c":
                    state = 1
                elif action == "q":
                    print("\nWins: " + str(wins) + "\nLosses: " + str(losses))
                    print("Thanks for playing.\n")
                    break

        except KeyboardInterrupt:
            print("Keyboard interrupt. Please don't do that again.")
